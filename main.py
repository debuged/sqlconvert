#!/usr/bin/env python2

import json

'''
# python 2
#import os.path
# python3 / python2 with backport
import pathlib
'''
import pathlib

import sys
if sys.version_info.major != 2:
    print("only python 2 supported")
    print("sorry bye")
    sys.exit(-1)

def run_alchemy(src_connection, dst_connection):
    from etlalchemy import ETLAlchemySource, ETLAlchemyTarget

    # Load ONLY the 'salaries' table
    source = ETLAlchemySource(src_connection)
    # Conversely, you could load ALL tables EXCEPT 'salaries'
    # source = ETLAlchemySource("mysql://,etlalchemy:etlalchemy@localhost/employees",\
    #                          excluded_tables=["salaries"])

    target = ETLAlchemyTarget(dst_connection, drop_database=True)
    target.addSource(source)
    target.migrate(migrate_fks=False, migrate_indexes=False, migrate_data=False, migrate_schema=True)


def read_Configuration(config_file):
    '''
    :param config_file:
    :return: config dict, validation status
    '''
    base_path = __file__ + "/../conf/" + config_file + ".json"
    '''
    # pure python 2 
    # real_path = os.path.abspath(base_path)
    '''
    real_path = str(pathlib.Path(base_path).resolve())

    with open(real_path, 'r') as f:
        data = json.load(f)
        is_valid = True
        for key in ("src", "dst"):
            if key not in data:
                print(key + " is missing !")
                is_valid = False
        return dict(data), is_valid
    # should never happen
    return none, False


if __name__ == '__main__':
    used_config_file = "default"
    if len(sys.argv) == 2:
        used_config_file = sys.argv[1]
    else:
        print("Usage: main.py <config_name>")
        print("Fall back to " + used_config_file)
        print("")
    print("Using config: " + used_config_file)
    config, config_valid = read_Configuration(used_config_file)
    if config_valid:
        run_alchemy(config["src"], config["dst"])
